module Run
  extend self

  # Formatea un run
  def self.format run, digit=false
    return nil if run.nil?
    pos = 0
    run.reverse
    reverse = ""
    chars = run.reverse.gsub("-", "").gsub(".", "").split('')
    chars.each_with_index do |c, index|
      reverse += c
      if index == 0
        reverse += '-'
      elsif (pos == 3) && index != chars.size - 1
        reverse += "."
        pos = 0
      end
      pos += 1
    end

    if digit
      reverse.reverse[0..-3]
    else
      reverse.reverse
    end
  end

  # Quita el formato de un run
  def self.remove_format run, digit=false
    return nil if run.nil?

    if digit
      run.gsub("-", "").gsub(".", "")[0..-3]
    else
      run.gsub("-", "").gsub(".", "")
    end
  end

  # Generar un run valido segun reglas chilenas y que ademas es unico para el modelo entregado
  def self.for model, attribute, randomize=true
    valid_rut = randomize ? self.generate : '111111111'

    model_class = model.to_s.camelize.constantize
    if model_class.respond_to?(:table_name) && ActiveRecord::Base.connection.table_exists?(model_class.table_name)
      while model_class.where(attribute.to_sym => valid_rut).any? do valid_rut = self.generate end
    else
      puts "not ActiveRecord::Base.connection.table_exists? model.to_s"
    end

    valid_rut
  end

  # Generar un run valido segun reglas chilenas
  def self.generate
    run = (rand * 20000000).round + 5000000

    suma = 0
    mul = 2
    run.to_s.reverse.split('').each do |i|
      suma = suma + i.to_i * mul
      if mul == 7
        mul = 2
      else
        mul += 1
      end
    end
    res = suma % 11

    if res == 1
      return "#{run}k"
    elsif res == 0
      return "#{run}0"
    else
      return "#{run}#{11-res}"
    end
  end

  # Revisa si el run entregado es valido, el run debe incluir el digito verificador
  def valid? run
    if run
      run_copy = run.clone
      run_copy = run_copy.match(/^(\d+)\.{1}\d{3}\.{1}\d{3}-{1}(\d|k|K)$|^(\d+)\d{6}\-{1}(\d|k|K)|^(\d+)\d{6}(\d|k|K)$/i).to_s
      return false unless run_copy
      run_copy = remove_format(run_copy)
      check_run run_copy.chop, run_copy.last
    else
      false
    end
  end

  private
  # Revisa si el run y digito entregado es valido
  def self.check_run run, digit
    return false if run.empty?
    return false if digit.empty?
    return false if run.length > 8 || run.length < 7

    factor = 2
    suma   = 0
    i      = run.length - 1

    while i >= 0
      factor = 2 if factor > 7
      suma   = suma + ( run[i] ).to_i * factor.to_i
      factor += 1
      i      -= 1
    end

    dv = 11 - ( suma % 11 )
    dv = 0    if dv == 11
    dv = "k"  if dv == 10

    return true if dv.to_s == digit.downcase
    false
  end
end
