# encoding: utf-8

class RunValidator < ActiveModel::Validator
  def validate record
    options[:attributes].each do |attribute|
      record.errors[attribute.to_sym] << 'no es válido' unless Run.valid?(record.send(attribute))
    end
  end

  private
  # TODO - locales
  # record.errors[attribute.to_sym] << I18n.t('run.invalid') unless Run.valid? record.send(attribute)
end