require 'run_cl/version'
require 'run_cl/run'
require 'run_cl/run_validator'
require 'run_cl/uniq_run_validator'


module RunCl

  module ActAsRun
    extend ActiveSupport::Concern

    # module InstanceMethods
    # end

    module ClassMethods
      # include InstanceMethods
      def has_run_cl(name, options = {})
        run                  = options.delete(:run)
        uniq_run             = options.delete(:uniq_run)
        skip_db_format_clear = options.delete(:skip_db_format_clear)

        if run.nil? or run
          validates name, options.merge({run: true})
        end

        if uniq_run.class == Hash and uniq_run.keys.any?
          validates name, options.merge({uniq_run: uniq_run})
        elsif uniq_run.nil? or uniq_run == true
          validates name, options.merge({uniq_run: true})
        end

        if skip_db_format_clear.nil? or skip_db_format_clear == false
          before_validation "make_#{name}_format!"

          define_method "make_#{name}_format!" do
            self.send("#{name}=", Run.remove_format(self.send("#{name}")) )
          end
        end
      end
    end
  end
end
